/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include<set>
using std::set;


// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter() {
	return s1+s2+s3;
}

unsigned long triangle::area() {
	// TODO: write this function.
	// Note: why is it okay to return an integer here?  Recall that
	// all of our triangles have integer sides, and are right triangles...
	int area = 0;
	unsigned long max = s1;
	if((s2 > max)) {
		max = s2;
	}if((s3 > max)) {
		max = s3;
	}
	if((s1 == max)) {
		area = (s2*s3)/2;
	}else if((s2 == max)) {
		area = ((s1*s3))/2;
	}else if((s3 == max)) {
		area = ((s2*s1))/2;
	}
	return area;
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2) {
	// TODO: write this function.
	int x;
	int y;
	int z;
	x = t1.s1;
	y = t1.s2;
	z = t1.s3;
	unsigned long sides1[3] = {x,y,z};
	sort(sides1,sides1+3);
	int g;
	int h;
	int i;
	g = t2.s1;
	h = t2.s2;
	i = t2.s3;
	unsigned long sides2[3] = {g,h,i};
	sort(sides2,sides2+3);
	if(sides1[0] == sides2[0] && sides1[1] == sides2[1] && sides1[2] == sides2[2]) {
		return true;
	}
	return false;
}

bool similar(triangle t1, triangle t2) {
	// TODO: write this function.
	int o,p,q;
	o = t1.s1;
	p = t1.s2;
	q = t1.s3;
	unsigned long side1[3] = {o,p,q};
	sort(side1, side1+3);
	int r,s,t;
	r = t2.s1;
	s = t2.s2;
	t = t2.s3;
	unsigned long side2[3] = {r,s,t};
	sort(side2, side2+3);
	if(side1[0]*side2[1] == side1[1]*side2[0] && side1[1]*side2[2] == side1[2]*side2[1]) {
		return true;
	}
	return false;
}

vector<triangle> findRightTriangles(unsigned long l, unsigned long h) {
	// TODO: find all the right triangles with integer sides,
	// subject to the perimeter bigger than l and less than h
	vector<int> a; //storage for the smallest side of the triangle
	vector<int> b; //storage for the second side of the triangle
	vector<int> c; //storage for the third side of the triangle
	vector<triangle> retvaltemp; //temporarily storage for triangle to remove repeated triangle
	vector<triangle> retval; //storage for return value
	for(size_t p = l; p <= h; p++) {
		for(size_t j = 1; j <= p/2; j++) {
			a.push_back(j);
			for(size_t k = p-1-j; k > 0; k--) {
				b.push_back(k);
				size_t m = p-j-k;
				c.push_back(m);
				if(((j + k)-m) == 1) {
					break;
				}
			}
			while(b.size() > a.size()) {
				a.push_back(j);
			//when the size of vector b > a
			//do push back a again
			}
		}
	}
	for(int i = 0;i < a.size(); i++) {
		size_t d = a[i];
		size_t e = b[i];
		size_t f = c[i];
		//push back into retval
		if(((d*d)+(e*e))==(f*f)) {
			retvaltemp.push_back(triangle(d,e,f));
		}
	}
	for(int i = 1;i <= retvaltemp.size(); i+=2) {
		retval.push_back(retvaltemp[i]);
	}
	return retval;
}

